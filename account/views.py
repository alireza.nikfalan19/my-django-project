from django.contrib.auth.views import LoginView, PasswordChangeView
from django.http import Http404
from django.shortcuts import render
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from blog.models import Article
from .mixins import FieldsMixin, FormValidMixin, AuthorAccessMixin
from .models import User
from .forms import ProfileForm


# Create your views here.


class ArticleList(LoginRequiredMixin, ListView):
    template_name = 'registration/home.html'

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Article.objects.all()
        else:
            return Article.objects.filter(author=self.request.user)


class ArticleCreat(LoginRequiredMixin, FormValidMixin, FieldsMixin, CreateView):
    template_name = 'registration/creatview.html'
    model = Article


class ArticleUpdate(AuthorAccessMixin, FormValidMixin, FieldsMixin, UpdateView):
    template_name = 'registration/creatview.html'
    model = Article


class ArticleDelete(DeleteView):
    model = Article
    success_url = reverse_lazy('account:homes')
    template_name = 'registration/article_delate.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        else:
            raise Http404("شما به این مقاله دسترسی ندارید")


class Profile(LoginRequiredMixin, UpdateView):
    model = User
    template_name = 'registration/profile.html'
    form_class = ProfileForm
    success_url = reverse_lazy("account:profile")

    def get_object(self):
        return User.objects.get(pk=self.request.user.pk)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()

        kwargs.update(
            {
                'user': self.request.user
            }
        )
        print(f'this kwargs {self.kwargs}')
        return kwargs


class Login(LoginView):
    def get_success_url(self):
        user = self.request.user

        if user.is_superuser or user.is_author:
            return reverse_lazy('account:homes')
        else:
            return reverse_lazy('account:profile')