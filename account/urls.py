# It is also provided as a convenience to those who want to deploy these URLs
# elsewhere.

app_name = 'account'

from django.contrib.auth import views
from django.urls import path
from .views import ArticleList
from .views import ArticleCreat, ArticleUpdate, ArticleDelete, Profile

urlpatterns = [
    path('account', ArticleList.as_view(), name='homes'),
    path('account/article/creat', ArticleCreat.as_view(), name='creat'),
    path('account/article/update/<int:pk>', ArticleUpdate.as_view(), name='update'),
    path('account/article/delate/<int:pk>', ArticleDelete.as_view(), name='delete'),
    path('account/profile', Profile.as_view(), name='profile'),
]
