from django.http import Http404
from django.shortcuts import get_object_or_404,redirect
from blog.models import Article


class FieldsMixin():
    """Verify that the current user is authenticated."""

    def dispatch(self, request, *args, **kwargs):
        self.fields = ["title", "category", "slug", "descriptions", "thumbnail", "publish", 'is_special',
                       "status"]

        if request.user.is_superuser:
            self.fields.append('author')
        return super().dispatch(request, *args, **kwargs)


class FormValidMixin():
    """editing the form to post """

    def form_valid(self, form):
        if self.request.user.is_superuser:
            form.save()
        else:
            self.obj = form.save(commit=False)
            self.obj.author = self.request.user
            if not self.obj.status == 'i':
                self.obj.status = 'd'
        return super().form_valid(form)



class AuthorAccessMixin():
    """Verify that the current user is authenticated."""

    def dispatch(self, request, pk, *args, **kwargs):
        article = get_object_or_404(Article, pk=pk)
        if article.author == request.user and article.status in ['d','b'] or request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        else:
            raise Http404("شما به این مقاله دسترسی ندارید")



class AuthorsAccessMixin():
    """Verify that the current user is authenticated."""

    def dispatch(self, request, pk, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.is_superuser or request.user.is_author:
                return super().dispatch(request, *args, **kwargs)
            else:
                return redirect('account:profile')
        else:
            return redirect('account:profile')

class SuperUserMixin():
    """Verify that the current user is authenticated."""

    def dispatch(self, request, pk, *args, **kwargs):
        if request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        else:
            raise Http404("شما به این مقاله دسترسی ندارید")
