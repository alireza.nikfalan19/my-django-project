from account.models import User
from django.db import models
from django.utils import timezone
import os
from extensions.utils import jalali_converter
from django.utils.html import format_html
from django.urls import reverse


class ArticleManager(models.Manager):

    def published(self):
        return self.filter(status='p')


class CategoryManager(models.Manager):

    def active(self):
        return self.filter(status=True)


# Create your models here.
class Category(models.Model):
    parnet = models.ForeignKey('self', default=None, null=True, blank=True, on_delete=models.SET_NULL,related_name='childern', verbose_name='زیر مجموعه ها', )
    title = models.CharField(max_length=200, verbose_name='عنوان')
    slug = models.SlugField(max_length=200, unique=True, verbose_name='ادرس دست بندی')
    status = models.BooleanField(default=True, verbose_name='نمایش شود/نشود')
    positions = models.IntegerField(default=True, blank=True, verbose_name='پوزیشن')

    class Meta:
        verbose_name = 'دسته بندی'
        verbose_name_plural = 'دسته بندی ها'
        ordering = ['positions']

    def __str__(self):
        return self.title

    objects = CategoryManager()


class Article(models.Model):
    STATUS_CHOICES = (
        ('d', 'منتشر نشده'),
        ('p', 'انتشار یافته'),
        ('i', 'قفل شده'),
        ('b', 'برگشت داد شده'),
    )
    title = models.CharField(max_length=200, verbose_name='عنوان')
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='article',
                               verbose_name='نویسنده')
    category = models.ManyToManyField(Category, verbose_name='دسنه بندی ها', related_name='article')
    slug = models.SlugField(max_length=200, unique=True, verbose_name='نام url')
    descriptions = models.TextField(verbose_name='توضیحات')
    thumbnail = models.ImageField(upload_to='images', verbose_name='عکس')
    publish = models.DateTimeField(default=timezone.now, verbose_name='زمان انتشار')
    created = models.DateTimeField(auto_now_add=True)  # Time to create a blog
    update = models.DateTimeField(auto_now=True)  # Blog update time
    is_special = models.BooleanField(default=False, verbose_name='مقاله ویژه')
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, verbose_name='وضعیت')

    class Meta:
        verbose_name = 'مقاله'
        verbose_name_plural = 'مقالات'
        ordering = ['-publish']

    def __str__(self):
        return self.title

    def jpu(self):
        return jalali_converter(self.publish)

    jpu.short_description = 'زمان انتشار'

    def category_published(self):
        return self.category.filter(status=True)

    def thumbnail_tag(self):
        return format_html("<img width=100 src='{}' style='border-radius:5px'>  ".format(self.thumbnail.url))

    def get_absolute_url(self):

        return reverse("account:homes")

    thumbnail_tag.short_description = 'عکس'

    def catgores(self):
        return ", ".join([categoryes.title for categoryes in self.category_published()])

    catgores.short_description = 'دسته بندی'

    objects = ArticleManager()

