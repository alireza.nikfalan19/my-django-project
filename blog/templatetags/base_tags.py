from django import template
from ..models import Category

register = template.Library()


@register.simple_tag
def title():
    return "وبلاگ جنگویی"


@register.inclusion_tag('parisial/category_navbar.html')
def Category_navbar():
    return {
        "catgoryes": Category.objects.all()
    }


@register.inclusion_tag("registration/partial/link.html")
def link(request, link_name, content, classes):
    return {
        "request": request,
        'link_name': link_name,
        'link': 'account:{}'.format(link_name),
        'content': content,
        'class': classes
    }
