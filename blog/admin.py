from django.contrib import admin
from .models import Article, Category
from django.contrib import messages
from django.utils.translation import ngettext


# def make_published(modeladmin,request,queryset):
#     updated = queryset.update(status='p')
#     if updated == 1:
#         message_bit = "1 مقاله منتشر شد"
#     else:
#         message_bit = "منتشر شد"
#     modeladmin.message_user(request, '{} مقاله {}'.format(updated, message_bit))
#
#
# make_published.short_description = 'انتشار مقالات'


def make_deraft(modeladmin, request, queryset):
    queryset.update(status='d')


make_deraft.short_description = 'پیش نویس مقالات'


# Register your models here.
class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'jpu', 'thumbnail_tag', 'status', 'author', 'is_special', 'slug', 'catgores')
    list_filter = ('publish', 'status', 'author')
    search_fields = ('title', 'descriptions')
    prepopulated_fields = {'slug': ('title',)}

    def make_published(self, request, queryset):
        updated = queryset.update(status='p')
        self.message_user(request, ngettext(
            '%d مقاله انتشار یافت.',
            '%d مقاله انتشار یافت.',
            updated,
        ) % updated, messages.SUCCESS)

    make_published.short_description = 'انتشار مقالات'

    actions = [make_published, make_deraft]


admin.site.register(Article, ArticleAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'status', 'slug', 'parnet')
    list_filter = (['status'])
    search_fields = ('title',)
    prepopulated_fields = {'slug': ('title',)}


admin.site.register(Category, CategoryAdmin)
