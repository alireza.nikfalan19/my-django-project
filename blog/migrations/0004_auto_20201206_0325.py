# Generated by Django 3.1.3 on 2020-12-06 11:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_auto_20201205_0838'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ['positions'], 'verbose_name': 'دسته بندی', 'verbose_name_plural': 'دسته بندی ها'},
        ),
        migrations.AddField(
            model_name='category',
            name='positions',
            field=models.IntegerField(blank=True, default=True, verbose_name='پوزیشن'),
        ),
    ]
