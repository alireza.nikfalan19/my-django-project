from account.models import User
from django.shortcuts import render
from .models import Article, Category
from django.http import Http404
from django.shortcuts import get_object_or_404, get_list_or_404
from django.core.paginator import Paginator
from django.utils import timezone
from django.views.generic import ListView, DetailView
from account.mixins import AuthorAccessMixin

# Create your views here.
# def home(request, page=1):
#     article = Article.objects.published()
#     paginator = Paginator(article, 1)
#     articles = paginator.get_page(page)
#     now = timezone.now()
#     context = {
#         'article': articles
#     }
#     return render(request, 'home.html', context)


class ArticleList(ListView):
    template_name = 'home.html'
    queryset = Article.objects.published()
    paginate_by = 1
    context_object_name = "article"


# def detail(request, slug, **kwargs):
#     article = get_object_or_404(Article.objects.published(), slug=slug, status='p')
#     m = get_list_or_404(Article)
#     print(m)
#     print(article)
#     context = {
#         'articles': article
#     }
#     return render(request, 'detail.html', context)


# def Categoryes(request, slug, page=1):
#     catgores = get_object_or_404(Category, slug=slug, status=True)
#     name = catgores.childern.all()
#     for cat in name:
#         print(f'my category : {cat}')
#     article = catgores.article.published()
#     paginator = Paginator(article, 1)
#     articles = paginator.get_page(page)
#     context = {
#         "category": catgores,
#         "article": articles
#     }
#     return render(request, 'category.html', context)


class ArticleDetailView(DetailView):
    template_name = 'detail.html'

    def get_object(self):
        slug = self.kwargs.get('slug')
        return get_object_or_404(Article.objects.published(), slug=slug)


class Article_preview(AuthorAccessMixin,DetailView):
    template_name = 'detail.html'

    def get_object(self):
        pk = self.kwargs.get('pk')
        return get_object_or_404(Article, pk=pk)


class Category_List(ListView):
    paginate_by = 1
    template_name = 'category.html'

    def get_queryset(self):
        global category
        slug = self.kwargs['slug']
        category = get_object_or_404(Category.objects.active(), slug=slug)
        return category.article.published()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = category
        return context


class Author_List(ListView):
    paginate_by = 1
    template_name = 'author.html'

    def get_queryset(self):
        global author
        username = self.kwargs['username']
        author = get_object_or_404(User, username=username)
        return author.article.published()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['author'] = author
        return context
