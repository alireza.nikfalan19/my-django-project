from django.urls import path
from .views import (
    Category_List,
    ArticleList,
    ArticleDetailView,
    Author_List,
    Article_preview
)

app_name = 'blog'

urlpatterns = [
    path('', ArticleList.as_view(), name='home'),
    path('page/<int:page>', ArticleList.as_view(), name='homes'),
    path('article/<slug:slug>', ArticleDetailView.as_view(), name='detail'),
    path('Cartegory/<slug:slug>', Category_List.as_view(), name='Category'),
    path('Cartegory/<slug:slug>/page/<int:page>', Category_List.as_view(), name='Categoryes'),
    path('author/<slug:username>', Author_List.as_view(), name='author'),
    path('author/<slug:username>/page/<int:page>', Author_List.as_view(), name='authores'),
    path('preview/<int:pk>', Article_preview.as_view(), name='preview'),

]
